# -*- coding: utf-8 -*-
# anynumber.py -- Python flexible numbers
# Copyright © 2011 Luke Dashjr
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, version 3 only.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import decimal as _decimal
import fractions as _fractions
import re as _re
import sys as _sys

### Portability hacks for supporting Python 2 and 3

for _c, _r in (
	('long', r'type(0)'),
	('unichr', r'chr'),
	('unicode', r'type(unichr(0xffff))'),
	('xrange', r'range'),
):
	try:
		eval(_c)
	except NameError:
		globals()[_c] = eval(_r)

_type_int = (type(0), long)
_type_real = (type(0.0),)
_type_str = (type(''), unicode)
_type_list = (type(()), type([]))
_type_number = _type_int + _type_real + (_decimal.Decimal,)

class _tmp:
	def __repr__(self):
		return unichr(0xffff)
	__str__ = __repr__
_tmp = _tmp()
try:
	repr(_tmp)
	def _fstr(s):
		return s
except UnicodeEncodeError:
	def _fstr(s):
		return s.encode('utf-8')
try:
	str(_tmp)
	_strUnicode = True
except UnicodeEncodeError:
	_strUnicode = False

try:
	_maxUC = _sys.maxunicode
except AttributeError:
	_maxUC = 0xffff
def unichror(cp, alt):
	if cp > _maxUC:
		return alt
	return unichr(cp)

_uNull = unicode()
_uFraction = unichror(0x2044, '/')
_uScriptE = unichror(0x2130, 'E')
_uScriptX = unichror(0x1d4b3, 'X')

try:
	unichror.func_code
	def _getfc(f):
		return f.func_code
except AttributeError:
	def _getfc(f):
		return f.__code__

### BASE NUMBER CLASS

class Number(object):
	def __new__(cls, value = 0):
		self = object.__new__(cls)
		if isinstance(value, (Number, _fractions.Fraction)):
			self.numerator = value.numerator
			self.denominator = value.denominator
		elif isinstance(value, _type_int):
			self.numerator = value.numerator
			self.denominator = 1
		elif isinstance(value, _type_real):
			n = value
			d = 1
			while n % 1:
				n *= 2
				d *= 2
			self.numerator = int(n)
			self.denominator = d
		elif isinstance(value, _type_list) and len(value) == 2:
			self.numerator = value[0]
			self.denominator = value[1]
		elif isinstance(value, _decimal.Decimal):
			sign, digits, exponent = value.as_tuple()
			self.numerator = sum(digits[-i - 1] * 10 ** i for i in xrange(len(digits)))
			if exponent > 0:
				self.numerator *= 10 ** exponent
				exponent = 0
			self.denominator = 10 ** -exponent
		else:
			n, d = self._parse(value)
			self.numerator = n
			self.denominator = d
		
		self._simplify()
		return self
	
	@staticmethod
	def _parse(value):
		raise TypeError("unsupported type(s): '%s'" % (type(value).__name__,))
	
	def _simplify(self):
		if type(self.numerator) is Number:
			if isinstance(self.denominator, _type_int + _type_real + (_decimal.Decimal,)):
				self.denominator = self.denominator * self.numerator.denominator
				self.numerator = self.numerator.numerator
	
	def __Urepr(self):
		try:
			s = ('%s' + _uFraction + '%s') % (self.__class__(self.numerator), self.__class__(self.denominator))
		except NotImplementedError:
			s = Number(self).__Urepr()
		s = '<%s number %s>' % (self.__class__.__name__, s)
		return s
	
	def __repr__(self):
		return _fstr(self.__Urepr())
	
	def __su(self, useUnicode):
		fc = _getfc(self.format)
		akw = fc.co_varnames[:fc.co_argcount]
		kw = {}
		if 'useUnicode' in akw:
			kw['useUnicode'] = useUnicode
		return self.format(**kw)
	
	def __str__(self):
		s = self.__su(_strUnicode)
		return _fstr(s)
	
	def __unicode__(self):
		return self.__su(True)
	
	def format(self, useUnicode = True):
		if self.denominator == 1:
			return "%s" % self.numerator
		return ('(%d' + (_uFraction if useUnicode else '/') + '%d)') % (self.numerator, self.denominator)
	
	def _precmp(self, other):
		if type(other) != Number:
			other = Number(other)
		a = self.numerator * other.denominator
		b = other.numerator * self.denominator
		return a, b
	
	def __lt__(self, other):
		a, b = self._precmp(other)
		return a < b
	
	def __le__(self, other):
		a, b = self._precmp(other)
		return a <= b
	
	def __eq__(self, other):
		a, b = self._precmp(other)
		return a == b
	
	# NOTE: Python 2 doesn't invert __eq__
	def __ne__(self, other):
		a, b = self._precmp(other)
		return a != b
	
	def __gt__(self, other):
		a, b = self._precmp(other)
		return a > b
	
	def __ge__(self, other):
		a, b = self._precmp(other)
		return a >= b
	
	def __nonzero__(self):
		return bool(self.numerator)
	__bool__ = __nonzero__
	
	def __len__(self):
		return len(self.format())
	
	def _unify(self, other):
		other = Number(other)
		a = self.numerator
		b = other.numerator
		d = self.denominator
		db = other.denominator
		if d != db:
			dx = _fractions.gcd(d, db)
			a *= db / dx
			b *= d / dx
			d *= db / dx
		return (a, b, d)
	
	def __add__(self, other):
		a, b, d = self._unify(other)
		return self.__class__((a + b, d))
	__radd__ = __add__
	
	def __sub__(self, other):
		a, b, d = self._unify(other)
		return self.__class__((a - b, d))
	
	def __rsub__(self, other):
		a, b, d = self._unify(other)
		return self.__class__((b - a, d))
	
	def __mul__(self, other):
		other = Number(other)
		n = self.numerator * other.numerator
		d = self.denominator * other.denominator
		return self.__class__((n, d))
	__rmul__ = __mul__
	
	def __pow__(self, other, modulo = None):
		# FIXME: lossy
		other = float(other) if other % 1 else long(other)
		n = self.numerator ** other
		d = self.denominator ** other
		return self.__class__((n, d))
	
	def __rpow__(self, other):
		other = Number(other)
		# FIXME: lossy
		self = float(self) if self % 1 else long(self)
		n = other.numerator ** self
		d = other.denominator ** self
		return self.__class__((n, d))
	
	def __truediv__(self, other):
		other = Number(other)
		n = self.numerator * other.denominator
		d = self.denominator * other.numerator
		return self.__class__((n, d))
	__div__ = __truediv__
	
	def __rtruediv__(self, other):
		other = Number(other)
		n = other.numerator * self.denominator
		d = other.denominator * self.numerator
		return self.__class__((n, d))
	__rdiv__ = __rtruediv__
	
	def __floordiv__(self, other):
		n = self.__truediv__(other)
		return n.numerator // n.denominator
	
	def __rfloordiv__(self, other):
		n = self.__rtruediv__(other)
		return n.numerator // n.denominator
	
	def __mod__(self, other):
		n = self.__truediv__(other)
		return n.numerator % n.denominator
	
	def __rmod__(self, other):
		n = self.__rtruediv__(other)
		return n.numerator % n.denominator
	
	def __lshift__(self, other):
		n = self.numerator * (2 ** other)
		d = self.denominator
		return self.__class__((n, d))
	
	def __rlshift__(self, other):
		other = Number(other)
		n = other.numerator * (2 ** self)
		d = other.denominator
		return self.__class__((n, d))
	
	def __rshift__(self, other):
		n = self.numerator
		d = self.denominator * (2 ** other)
		return self.__class__((n, d))
	
	def __rrshift__(self, other):
		other = Number(other)
		n = other.numerator
		d = other.denominator * (2 ** self)
		return self.__class__((n, d))
	
	def __and__(self, other):
		raise NotImplemented()
	def __rand__(self, other):
		raise NotImplemented()
	def __xor__(self, other):
		raise NotImplemented()
	def __rxor__(self, other):
		raise NotImplemented()
	def __or__(self, other):
		raise NotImplemented()
	def __ror__(self, other):
		raise NotImplemented()
	
	def __neg__(self):
		n = self.numerator
		d = self.denominator
		return self.__class__((-n, d))
	
	def __pos__(self):
		return self
	
	def __abs__(self):
		return self if self.numerator >= 0 else -self
	
	def __invert__(self):
		raise NotImplemented()
	
	def __complex__(self):
		raise NotImplemented()
	
	def __long__(self):
		return self.numerator // self.denominator
	__index__ = __long__
	__int__ = __long__
	
	def __float__(self):
		return float(self.numerator) / self.denominator
	
	def __oct__(self):
		raise NotImplemented()
	def __hex__(self):
		raise NotImplemented()

_type_number += (Number,)

### UTILITY

Naught = Number(0)

def _commafy(s, groupsize, c = ','):
	n = ''
	if s[0] == '-':
		n = s[0]
		s = s[1:]
	firstcomma = len(s) % groupsize or groupsize
	n += s[:firstcomma]
	r = s[firstcomma:]
	segments = (n,) + tuple(r[i:i+groupsize] for i in range(0, len(r), groupsize))
	return c.join(segments)

def _doInteger(n, radix, digitf):
	s = _uNull
	i = abs(long(n))
	while True:
		s = digitf(i % radix) + s
		i //= radix
		if not i:
			break
	return s

def _doDelimiters(s, wantDelimiters, groupsize = 3, c = ','):
	if not wantDelimiters:
		return s
	if wantDelimiters is True:
		pass
	elif isinstance(wantDelimiters, _type_str):
		c = wantDelimiters
	elif isinstance(wantDelimiters, _type_int + (_decimal.Decimal, Number)):
		groupsize = wantDelimiters
	elif isinstance(wantDelimiters, _type_list):
		c = wantDelmiters[0]
		groupsize = wantDelimiters[1]
	return _commafy(s, groupsize, c)

def _sign(n, addSign = False):
	if n < 0:
		return '-'
	elif addSign:
		return '+'
	return ''

def _doFraction(num, radix, digitf, point = '.', maxPrecision = 100):
	n = num.numerator
	d = num.denominator
	
	s = _uNull
	n = abs(n) % d
	if n:
		s += point
		i = 0
		while n and i < maxPrecision:
			n *= radix
			s += digitf(n // d)
			n %= d
			i += 1
	return s

def _doFormat(num, radix, digitf, delim, point = '.', maxPrecision = 100, wantDelimiters = False, addSign = False):
	n = num.numerator
	d = num.denominator
	
	# Whole number part
	s = _doInteger(abs(n) // d, radix, digitf)
	
	# Add delimiters and sign
	s = _doDelimiters(s, wantDelimiters, *delim)
	s = _sign(n, addSign) + s
	
	# Fractional part
	s += _doFraction(num, radix, digitf, point=point, maxPrecision=maxPrecision)
	
	return s

def _parseRadix(value, parser, intf, base, numLen = len):
	m = parser(value.strip())
	if m is None:
		raise ValueError("Failed to parse: %s" % (value,))
	
	i = m.group('integer')
	i = intf(i) if i else 0
	
	f = m.group('fraction')
	if not f is None:
		e = numLen(f)
		d = base ** e
		i *= d
		i += intf(f)
	else:
		d = 1
	
	e = m.group('exponent')
	e = intf(e) if e else 0
	dx = base ** abs(e)
	if e < 0:
		d *= dx
	else:
		i *= dx
	
	if m.group('negative'):
		i = -i
	
	return (i, d)

def _calcDefMaxPrec(radix):
	defMaxPrec = 0
	tmp = 10e28
	while tmp:
		defMaxPrec += 1
		tmp //= radix
	return defMaxPrec

### UNARY

_unaryParser = _re.compile(r"""
	^
	(?P<negative>-)?
	(?P<integer>\S+)
	$
""", _re.VERBOSE | _re.IGNORECASE).match

class Unary(Number):
	digitre = '[1I|]'
	
	@staticmethod
	def _parse(value):
		value = value.strip()
		negative = False
		if value[0:1] in ('-', '+'):
			negative = value[0] == '-'
			if value[1:2] != value[0]:
				value = value[1:]
		
		i = len(value)
		
		if value == '0':
			i = 0
		
		if negative:
			i = -i
		
		d = 1
		
		return (i, d)
	
	def format(self, addSign = False, wantDelimiters = False, strictUnary = False):
		n = self.numerator
		d = self.denominator
		n //= d
		s = '|' * n if n else '' if strictUnary else '0'
		s = _doDelimiters(s, wantDelimiters)
		s = _sign(n, addSign) + s
		return s

### BINARY

_binaryParser = _re.compile(r"""
	^
	(?P<negative>-)?
	(?=\.?[01])
	(?P<integer>[01]*)
	(?:\.(?P<fraction>[01]*))?
	(?:e(?P<exponent>[+-]?[01]+))?
	$
""", _re.VERBOSE | _re.IGNORECASE).match

class Binary(Number):
	base = 2
	digitre = '[01]'
	
	@staticmethod
	def _parse(value):
		return _parseRadix(value, _binaryParser, lambda s: long(s, 2), 2)
	
	def format(self, addSign = False, wantDelimiters = False, maxPrecision = 94):
		n = self.numerator
		d = self.denominator
		n //= d
		s = bin(abs(long(n)))[2:]
		s = _doDelimiters(s, wantDelimiters, 4)
		s = _sign(n, addSign) + s
		s += _doFraction(self, 2, str, maxPrecision=maxPrecision)
		return s

# TODO: http://en.wikipedia.org/wiki/Non-adjacent_form

### TRINARY, QUATERNARY, QUINARY, SENARY, SEPTENARY, NONARY

def _generateSubdeci(name, radix, defGS = 3):
	digits = ''.join(map(str, range(radix)))
	digitre = '[%s]' % (digits,)
	parser = _re.compile(r"""
		^
		(?P<negative>-)?
		(?=\.?""" + digitre + r""")
		(?P<integer>""" + digitre + r"""*)
		(?:\.(?P<fraction>""" + digitre + r"""*))?
		(?:e(?P<exponent>[+-]?""" + digitre + r"""+))?
		$
	""", _re.VERBOSE | _re.IGNORECASE).match
	
	defMaxPrec = _calcDefMaxPrec(radix)
	
	class subclass(Number):
		base = digits
		@staticmethod
		def _parse(value):
			return _parseRadix(value, parser, lambda s: long(s, radix), radix)
		
		def format(self, maxPrecision = defMaxPrec, **kwargs):
			return _doFormat(self, radix, str, (defGS,), maxPrecision=maxPrecision, **kwargs)
	
	subclass.__name__ = name
	subclass.digitre = digitre
	
	globals()[name] = subclass

_generateSubdeci('Trinary', 3)
_generateSubdeci('Quaternary', 4, 4)
_generateSubdeci('Quinary', 5)
_generateSubdeci('Senary', 6)
_generateSubdeci('Septenary', 7)
_generateSubdeci('Nonary', 9)

### OCTAL

_octalParser = _re.compile(r"""
	^
	(?P<negative>-)?
	(?=\.?[0-7])
	(?P<integer>[0-7]*)
	(?:\.(?P<fraction>[0-7]*))?
	(?:e(?P<exponent>[+-]?[0-7]+))?
	$
""", _re.VERBOSE | _re.IGNORECASE).match

class Octal(Number):
	base = 8
	digitre = '[0-7]'
	
	@staticmethod
	def _parse(value):
		return _parseRadix(value, _octalParser, lambda s: long(s, 8), 8)
	
	def format(self, addSign = False, wantDelimiters = False, maxPrecision = 32):
		n = self.numerator
		d = self.denominator
		n //= d
		s = oct(abs(n))[1:].rstrip('L')
		if s == '': s = '0'
		s = _doDelimiters(s, wantDelimiters)
		s = _sign(n, addSign) + s
		s += _doFraction(self, 8, str, maxPrecision=maxPrecision)
		return s

### DECIMAL

_decimalParser = _re.compile(r"""
	^
	(?P<negative>-)?
	(?=\.?\d)
	(?P<integer>\d*)
	(?:\.(?P<fraction>\d*))?
	(?:e(?P<exponent>[+-]?\d+))?
	$
""", _re.VERBOSE | _re.IGNORECASE | _re.UNICODE).match

class Decimal(Number):
	base = 10
	digitre = '\d'
	
	@staticmethod
	def _parse(value):
		return _parseRadix(value, _decimalParser, int, 10)
	
	def format(self, addSign = False, wantDelimiters = False, wantScientific = False, maxPrecision = 28):
		with _decimal.localcontext() as ctx:
			ctx.prec = maxPrecision
			n = abs(self.numerator) // self.denominator
			s = str(long(n))
			if self.numerator < 0:
				s = "-%s" % (s,)
			fn = _decimal.Decimal(abs(self.numerator) % self.denominator) / self.denominator
			s += str(fn + 1)[1:]
		if n < 0 and long(n) == 0:
			s = '-' + s
		s = _doDelimiters(s, wantDelimiters)
		if addSign and n >= 0:
				s = "+" + s
		return s

### UNDECIMAL, DUODECIMAL, DOZENAL

_abc = '0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ'
def _generateUnicodeI(name, radix, defGS = 3, dc = ',', rp = '.'):
	if isinstance(radix, _type_list):
		digitsU = radix[0]
		digitsA = radix[1]
	else:
		digitsU = radix
		digitsA = None
	if isinstance(radix, _type_int):
		digitsU = ''.join(map(lambda i: _abc[i], range(radix)))
	else:
		radix = len(digitsU)
	if digitsA is None or digitsA == digitsU:
		digitsA = digitsU
		digits = digitsU
		toASCII = None
	else:
		digits = digitsA + digitsU
		digitsU = unicode(digitsU)
		toASCII = {}
		for i in xrange(len(digitsU)):
			toASCII[ord(digitsU[i])] = unicode(digitsA[i])
	digitre = '[%s]' % (digits,)
	parser = _re.compile(r"""
		^
		(?P<negative>-)?
		(?=\.?""" + digitre + r""")
		(?P<integer>""" + digitre + r"""*)
		(?:""" + _re.escape(rp) + r"""(?P<fraction>""" + digitre + r"""*))?
		(?:e(?P<exponent>[+-]?""" + digitre + r"""+))?
		$
	""", _re.VERBOSE | _re.IGNORECASE).match
	
	defMaxPrec = _calcDefMaxPrec(radix)
	
	def parseint(s):
		n = 0
		for i in xrange(len(s)):
			n *= radix
			n += digitsA.index(s[i].upper())
		return n
	parseint.__name__ = 'parseint(%s)' % name
	
	class subclass(Number):
		base = radix
		@staticmethod
		def _parse(value):
			if toASCII:
				value = unicode(value.strip()).translate(toASCII)
			return _parseRadix(value, parser, parseint, radix)
		
		def format(self, useUnicode = True, maxPrecision = defMaxPrec, **kwargs):
			ds = digitsU if useUnicode else digitsA
			return _doFormat(self, radix, lambda n: ds[n], (defGS, dc), point=rp, maxPrecision=maxPrecision, **kwargs)
	subclass.__name__ = name
	subclass.digitre = digitre
	
	return subclass

def _generateUnicode(name, *args, **kwargs):
	subclass = _generateUnicodeI(name, *args, **kwargs)
	globals()[name] = subclass

# TODO: DecimalBijective
_generateUnicode('UndecimalA', 11)
_generateUnicode('UndecimalX', ('0123456789' + _uScriptX, '0123456789X'))
_generateUnicode('Duodecimal', 12, 4)
_generateUnicode('Dozenal', ('0123456789' + _uScriptX + _uScriptE, '0123456789XE'), 4, ' ', ';')
_generateUnicode('Tridecimal', 13)
Tredecimal = Tridecimal
Triskadecimal = Tridecimal
_generateUnicode('Tetradecimal', 14)
_generateUnicode('Pentadecimal', 15)
Quindecimal = Pentadecimal
_generateUnicode('Vigesimal', 20)
_generateUnicode('Tetravigesimal', '0123456789ABCDEFGHJKLMNP')
_generateUnicode('HexavigesimalP', 26)
_generateUnicode('HexavigesimalZ', 'ABCDEFGHIJKLMNOPQRSTUVWXYZ')
# TODO: HexavigestimalBijective, A=1, A..Z,AA..AZ,BA..BZ,...
_generateUnicode('SeptemvigesimalQ', 26)
Septemvigesimal = SeptemvigesimalQ
_generateUnicode('SeptemvigesimalZ', ' ABCDEFGHIJKLMNOPQRSTUVWXYZ')
_generateUnicode('Trigesimal', 30)
_generateUnicode('Duotrigesimal', 32)
# TODO: Base32 encoding? A..Z,2..7, padding with =
_generateUnicode('Sexatrigesimal', 36)
Hexatridecimal = Sexatrigesimal
Hexatrigesimal = Sexatrigesimal
Alphadecimal = Sexatrigesimal
# TODO: Tetrasexagesimal/Base64?
# TODO: Ascii85?

def Base(radix):
	if radix in Base._cache:
		return Base._cache[radix]
	name = 'Base(%d)' % (radix,)
	subclass = _generateUnicodeI(name, radix)
	Base._cache[radix] = subclass
	return subclass
Base._cache = {}

### HEXADECIMAL

_hexadecimalParser = _re.compile(r"""
	^
	(?P<negative>-)?
	(?=\.?[\da-f])
	(?P<integer>[\da-f]*)
	(?:\.(?P<fraction>[\da-f]*))?
	(?:e(?P<exponent>[+-]?[\da-f]+))?
	$
""", _re.VERBOSE | _re.IGNORECASE).match

class Hexadecimal(Number):
	base = 0x10
	digitre = '[\da-f]'
	
	@staticmethod
	def _parse(value):
		return _parseRadix(value, _hexadecimalParser, lambda s: long(s, 0x10), 0x10)
	
	def format(self, addSign = False, wantDelimiters = False, maxPrecision = 24):
		n = self.numerator
		d = self.denominator
		s = '%x' % (abs(n) // d,)
		s = _doDelimiters(s, wantDelimiters, 4)
		s += _doFraction(self, 0x10, lambda n: '%x' % n, maxPrecision=maxPrecision)
		s = _sign(n, addSign) + s
		return s

### TONAL

_toTonalDict = {57: 0xe9d9, 65: 0xe9da, 66: 0xe9db, 67: 0xe9dc, 68: 0xe9dd, 69: 0xe9de, 70: 0xe9df, 97: 0xe9da, 98: 0xe9db, 99: 0xe9dc, 100: 0xe9dd, 101: 0xe9de, 102: 0xe9df}
_fromTonalDict = {0xe9d9: 57, 0xe9da: 97, 57: 97, 0xe9db: 98, 0xe9dc: 99, 0xe9dd: 100, 0xe9de: 101, 0xe9df: 102}

class Tonal(Hexadecimal):
	base = 0x10
	digitre = '[\d\\xe9d9-\\xe9df]'
	
	@staticmethod
	def _parse(value):
		value = unicode(value).translate(_fromTonalDict)
		return Hexadecimal._parse(value)
	
	def format(self, **kwargs):
		s = Hexadecimal.format(self, **kwargs)
		s = unicode(s).translate(_toTonalDict)
		return s

### SEXAGESIMAL

_sexagesimalDigits = map(lambda i: "%d:" % i, xrange(60))
_sexagesimalSep = _re.compile('[;:]')
_sexagesimalParser = _re.compile(r"""
	^
	(?P<negative>-)?
	(?=\.?[\d:;])
	(?P<integer>[\d:;]*)
	(?:\.(?P<fraction>[\d:;]*))?
	(?:e(?P<exponent>[+-]?[\d:;]+))?
	$
""", _re.VERBOSE | _re.IGNORECASE).match
_sexagesimalLen = lambda s: len(_sexagesimalSep.findall(s)) + 1
_sexagesimalSplit = _sexagesimalSep.split

def _sexagesimalInt(s):
	n = 0
	digits = _sexagesimalSplit(s)
	for d in digits:
		n *= 60
		d = int(d) if len(d) else 0
		if d > 59:
			raise ValueError("Found digit %d > 59 in: %s" % (d, s))
		n += d
	return n

class Sexagesimal(Number):
	base = 60
	digitre = '(?:\d+\:?)'
	separator = ':'
	
	@staticmethod
	def _parse(value):
		return _parseRadix(value, _sexagesimalParser, _sexagesimalInt, 60, numLen = _sexagesimalLen)
	
	def format(self, addSign = False, maxPrecision = 16):
		n = self.numerator
		d = self.denominator
		n //= d
		
		digitf = lambda n: _sexagesimalDigits[n]
		s = _doInteger(n, 60, digitf)[:-1]
		s = _sign(n, addSign) + s
		s += _doFraction(self, 60, digitf, maxPrecision=maxPrecision)[:-1]
		
		return s

### ROMAN NUMERALS

_romanDigits = {
	'I': 1,
	'i': 1,
	'|': 1,
	0x2160: 1,
	0x2161: (2, 1),
	0x2162: (3, 1),
	0x2163: (4, 5),
	'V': 5,
	'v': 5,
	0x039b: 5,
	0x2164: 5,
	0x2165: (6, 1),
	0x2185: 6,
	0x2166: (7, 1),
	0x2167: (8, 1),
	0x2168: (9, 10),
	'X': 10,
	'x': 10,
	0x2169: 10,
	0x216a: (11, 1),
	0x216b: (12, 1),
	'L': 50,
	0x216c: 50,
	'l': 50,
	0x22d4: 50,
	0x2186: 50,
	'C': 100,
	'c': 100,
	'8': 100,
	0x216d: 100,
	'D': 500,
	'd': 500,
	0x216e: 500,
	'M': 1000,
	'm': 1000,
	0x2295: 1000,
	0x216f: 1000,
	0x2180: 1000,
	0x2181: 5000,
	0x2182: 10000,
	0x2187: 50000,
	0x2188: 100000,
}
for _lc in xrange(0x10):
	_romanDigits[0x2170 + _lc] = _romanDigits[0x2160 + _lc]

_romanFractions = {
	'.': Number((1, 12)),
	':': Number((1, 6)),
	0x2234: Number((1, 4)),
	0x03a3: Number((1, 24)),
	0x0186: Number((1, 48)),
	0x01a7: Number((1, 72)),
	0x01bb: Number((1, 144)),
	0x2108: Number((1, 288)),
	0x00bb: Number((1, 1728)),
}
_romanFractions[0x2022] = _romanFractions['.']
_romanFractions[';'] = _romanFractions[':']
_romanFractions[0x00a3] = _romanFractions[0x2234]
_romanFractions[0x0404] = _romanFractions[0x2234]

for _udl in (_romanDigits, _romanFractions):
	for _i in _udl.keys():
		if not isinstance(_i, _type_int):
			continue
		if _i <= _maxUC:
			_j = unichr(_i)
			_udl[_j] = _udl[_i]
		del _udl[_i]

_romanDigitre = ''.join(tuple(_romanDigits.keys()) + tuple(_romanFractions.keys()))
_romanParser = _re.compile(r"""
	^
	(?P<negative>-)?
	(?P<integer>(?:(?:[""" + ''.join(_romanDigits.keys()) + r"""]+\))+|N|(?=.)))
	(?P<fraction>[""" + ''.join(_romanFractions.keys()) + r"""]+)?
	$
""", _re.VERBOSE | _re.IGNORECASE).match

class RomanNumeral(Number):
	digitre = _romanDigitre
	
	@staticmethod
	def _parse(value):
		m = _romanParser(value.strip())
		if m is None:
			raise ValueError("Failed to parse: %s" % (value,))
		
		i = 0
		ist = m.group('integer')
		ld = None
		ps = []
		for j in xrange(len(ist)):
			digit = ist[j]
			if digit in _romanDigits:
				digit = _romanDigits[digit]
				if type(digit) in _type_list:
					nld = digit[1]
					digit = digit[0]
				else:
					nld = digit
			elif digit == '(':
				ps.append((i, ld))
				i = 0
				ld = None
				continue
			elif digit == ')':
				digit = i * 1000
				i, ld = ps.pop()
			elif digit in ('N', 'n') or _re.match(r'\s', digit):
				continue
			if ld and digit > ld:
				i = digit - i
			else:
				i += digit
			ld = nld
		
		i = Number(i)
		fs = m.group('fraction')
		if not fs is None:
			for j in xrange(len(fs)):
				digit = fs[j]
				digit = _romanFractions[digit]
				i += digit
		
		if m.group('negative'):
			i = -i
		
		return (i.numerator, i.denominator)
	
	def format(self, addSign = False, maxPrecision = 16):
		raise NotImplementedError()

### MIXED RADIX

def MixedRadix(*radices):
	if radices in MixedRadix._cache:
		return MixedRadix._cache[radices]
	name = 'MixedRadix(%s)' % (', '.join(c.__name__ for c in radices),)
	for c in radices:
		try:
			Naught >= c.base
		except Error:
			raise ValueError("%s does not have a defined base" % (c.__name__,))
	
	radiceClasses = radices
	class subclass(Number):
		@staticmethod
		def _parse(value):
			radices = radiceClasses
			valueIn = value
			value = value.strip()
			negative = False
			if value[0:1] in ('-', '+'):
				negative = value[0] == '-'
				value = value[1:]
			
			i = 0
			radices = list(radices)
			rmult = 1
			while value:
				if radices:
					currentRadix = radices.pop()
				if hasattr(currentRadix, 'separator') and currentRadix.separator != '':
					valueLen = len(value)
					sep = currentRadix.separator
					digitLen = valueLen
					try:
						digitLen = digitLen - value.rindex(sep) - len(sep)
					except ValueError:
						pass
					digit = value[-digitLen:]
					value = value[:-digitLen - 1]
				else:
					digit = value[-1:]
					value = value[:-1]
				n = currentRadix(digit)
				i += rmult * n
				rmult *= currentRadix.base
			
			if negative:
				i = -i
			
			return (i.numerator, i.denominator)
		
		def format(self, **kwargs):
			return Number.format(self, **kwargs)
			ds = digitsU if useUnicode else digitsA
			return _doFormat(self, radix, lambda n: ds[n], (), maxPrecision=maxPrecision, **kwargs)
	subclass.__name__ = name
	
	MixedRadix._cache[radices] = subclass
	return subclass
MixedRadix._cache = {}

# TODO: Negative bases
# TODO: Quater-imaginary base
# TODO: Twindragon base
# TODO: bases φ, e, π, τ, and √2
# TODO: Nullary?
# TODO: Mixed radix
# TODO: Roman numerals
